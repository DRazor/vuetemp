import Vue from 'vue'
import App from './App.vue'

import Routes from './route.js'
import {store} from './store/store.js'
// import VueMaterial from 'vue-material'
import VueRouter from 'vue-router'
// import Vuex from 'vuex'

// Vue.use(Vuex)
// const store= new Vuex.Store({
//     state:{
//     	"name":"dSecret"
//     }
// })

// Vue.use(VueMaterial)
Vue.use(VueRouter)

const router =new VueRouter({
  routes:Routes,
  /*mode:'history'*/
});

new Vue({
  el: '#app',
  render: h => h(App),
  router:router,
  store:store
})
